---
title: "Continuous Delivery with Micro Focus"
date: 2020-08-01T13:26:08-07:00
lastmod: 2020-08-01T13:26:08-07:00
draft: false
tags:
  - ci/cd
  - microfocus
---

Webinar (delivered Summer 2020) showcasing Micro Focus Deployment Automation
adding continuous delivery to Micro Focus ALM Octane CI/CD pipelines.

## Source

[brighttalk.com](https://www.brighttalk.com/webcast/8653/424227/enable-continuous-delivery-with-micro-focus-deployment-automation-and-alm-octane)

## Technologies

- Automated Deployments in CI/CD Pipelines
  - Micro Focus ALM Octane
  - Micro Focus Deployment Automation
  - Micro Focus UFT Developer
  - Jenkins
- DevOps tool integrations
  - Jira
  - Selenium
