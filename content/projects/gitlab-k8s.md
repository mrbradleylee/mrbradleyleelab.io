---
title: "GitLab in Kubernetes"
date: 2021-12-21T12:39:03-07:00
draft: false
showToc: true
tags:
  - gitlab
  - kubernetes
  - helm
  - gcp
  - deprecated
---

> This instance has since been deprecated to reassign the domain name, which
> accurately points to the recent Omnibus based install.

Self hosted GitLab implementation using the default Helm chart, and customized
to include GitLab Runners and hosted environments in a single cluster
(architecture not recommended for production environments, see
[reference architecture](https://docs.gitlab.com/ee/administration/reference_architectures/)
for recommendations). Primary demo environment, maintained on current release.

## Source

[gitlab.blee.dev](https://gitlab.blee.dev)

## Technologies

- GitLab
- Kubernetes
  - Google Cloud
  - GKE
- Helm

### Tools

- Kubectl
  - Kubectx
  - Kubens

### References

🔗:
[https://docs.gitlab.com/charts/quickstart/index.html](https://docs.gitlab.com/charts/quickstart/index.html)

📹:
[https://www.youtube.com/watch?v=XcJqIggsJ5E](https://www.youtube.com/watch?v=XcJqIggsJ5E)

## Installation Steps

### Create GKE Cluster

This config uses `4` nodes of `n1-standard-2`, with `auto-provision` off, and
`auto-scaling` on and configured to min of `1` and max of `4`. This
configuration hits the recommended requirements of `8` CPU and `30` GB Memory.

Create static IP if needed.

This instance used a custom namespace in helm install: `gitlab`

I initially created the cluster with only `3` nodes, which needed to be expanded
to `4`. It can be expanded with:

```shell
gcloud container clusters resize <cluster-name> \
--node-pool <node-pool-name> \
--num-nodes 4 \
--zone=<zone>
```

### Install via Helm

The install command below assumes the following:

- Replace `example.com` with either `<ip>.nip.io` or reserved domain name
  - I have `blee.dev`, which should result in `https://gitlab.blee.dev`
- Replace `10.10.10.10` externalIP with static IP reserved in GCP
  - This tells the ingress to set up for forwarding rule automatically
- Replace `me@example.com` with a valid email for the SSL certs
- Namespace `-n` = `gitlab` if you don't want to use the default namespace
- Setting `global.kas.enabled` installs the GitLab Kubernetes Agent Server
  - Certificate based cluster integrations were deprecated in `14.5` release
- Setting `gitlab-runner.runners.privileged` allows Docker and Kubernetes based
  runners to run in `privileged` mode, along with the insecurities that come
  along with it...

```shell
helm repo add gitlab https://charts.gitlab.io

helm repo update

helm upgrade --install gitlab gitlab/gitlab \
  --timeout 600s \
  --set global.hosts.domain=example.com \
  --set global.hosts.externalIP=10.10.10.10 \
  --set global.ingress.configureCertmanager=true \
  --set certmanager-issuer.email=me@example.com \
  --set global.kas.enabled=true \
  --set global.hosts.https=true
  --set gitlab-runner.runners.privileged=true
  -n gitlab
```

### Fetch Initial Root Password

Once that install completes, the following command will output the initial root
password. GitLab should be accessible through the ingress IP, and the password
will need to be reset on first login.

```shell
kubectl get secret -n gitlab gitlab-gitlab-initial-root-password \
-ojsonpath='{.data.password}' | base64 --decode ; echo
```

## Upgrades

Upgrading the installation (22nd of every month 😉) is very straightforward:

- Output your current values to a `gitlab.yaml` file:

```shell
helm get values gitlab > gitlab.yaml
```

- Update your helm repo with the updated charts:

```shell
helm repo update
```

- Get the
  [version mapping](https://docs.gitlab.com/charts/installation/version_mappings.html)
- Upgrade via helm:

```shell
helm upgrade gitlab gitlab/gitlab --version 5.8.0 -f gitlab.yaml
```

## Conclusion

This should get you a running GitLab environment in a Kubernetes cluster, ready
for any shenanigans you're ready to put to it. In order to integrate
groups/projects with the cluster, you'll need to install the GitLab Agent for
Kubernetes which we'll cover at a later time.
