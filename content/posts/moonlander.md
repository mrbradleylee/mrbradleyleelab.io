---
title: "Day One With The ZSA Moonlander"
date: 2022-03-04T09:00:39-07:00
lastmod: 2022-03-04T09:00:39-07:00
tags:
  - moonlander
  - keyboards
  - ortholinear
---

Struggle through learning split ergo typing with me...

## WTH Is A Moonlander? 🧑‍🚀

This is going to be a short one, since this is pretty much day one with this
thing... What is a Moonlander, you might ask? Great question... The
[Moonlander](https://www.zsa.io/moonlander/) is a split ergonomic keyboard with
an admittedly funky layout, and less keys than I'm used to using... I've read a
fair bit about ergo keyboards and their benefits, so I figured I'd try one out
and see what all the fuss is about.

## Why The Moonlander?

Perhaps a better question, why an ergo at all? For me, the answer was simple. I
spend a lot of time typing... I've started to notice some pain in my hands and
wrists this year so I figured let's start down this rabbit hole.

I narrowed down to a few, which included the ErgoDox EZ, and the Kinesis models.
I settled on the Moonlander for a few reasons:

- I'm a fan of wrist rests, and standard wrist rests don't really work for split
  keyboards. I like the fact that the Moonlander included folding wrists rests.
- Smaller thumb keypads
  - The EZ had a pretty big thumb cluster. I like thumb modifiers, but they'll
    take a bit of adjustments to get to.

### The Default Build

The Moonlander by default comes with a hot swappable PCB, with OEM R3 keycaps.
All keys in the main area are 1u sized, while the thumb cluster contains 3x 1.5u
modifiers and a 2u on each side. The 2u is a strange shape that can be
challening to replace since it also uses stabilizers to attach the keycap. It
comes with a variety of switch choices, and I chose the good old standby Cherry
MX Browns.

### Fully Programmable

One of the big draws of the Moonlander is that it's FULLY programmable, using
QMK firmware. You can put pretty much anything on any key (to and extent, of
course), including macros. Since the Moonlander is basically a 60% keyboard,
you're giving up quite a bit of keys, and since I'm primarily on MacOS, I'm
giving up some modifiers as well. Being fully programmable helps replace some
missing basics, but also presents quite the learning curve...

### Layer Support

Through the web-based Oryx config tool (based on QMK), I'm able to create a
bunch of layers on the keyboard accessed via modifiers (also configurable).
There isn't a standard row of function keys, which I do use a fair bit, but the
largest adjustment so far is the symbols, which I've placed on a second layer.
This also offsets the lack of a traditional arrow pad, with which I definitely
prefer the Vim style `HJKL` navigation and set those on the second layer as
well.

## Things I Don't Like

It's not all sugar and rainbows here. I think I'd prefer the larger side keys on
the EZ, as so far it feels a little odd hitting a `Shift` key that's the size of
a standard 1u key. So far (and this probably just needs time) I'm not enjoying
the columnar layout of the keyboard. Perhaps it's just bad typing habits (it's
definitely bad typing habits), but I'm having a ton of issues with the `xcv`
cluster of keys.

It also seems that my left hand has now taken on so much with a single space key
and moving the `Enter` key to the thumb cluster. I'm positive this is going to
take a few weeks at best to get used to, but we're up for the challenge!

## The Initial Adjustment Period

Here are a few things that I've noticed have helped me:

### Patienve

Yes, I left that typo there as it pretty much happens every time I reach for the
`c` key. Although as I'm typing this, I think it may be related to the position
of the keypads. I've been typing on a staggered layout for 20+ years, so it's
hard to stop extending that reach for that cluster... This will probably be the
biggest barrier to entry for me, and may eventually be the proverbial straw that
broke the camel's back.

### Layouts

I've kept the majority of the default layout for now, but I can definitely see
some changes I want to make. The particularly hard part will be getting used to
where the new symbols will be. I'm guessing I never really associated the `+`
sign with the `=` key until I started typing on this thing. You always thought
you were a touch typist until you were actually forced to do it.

## Value Realization

I thought the initial feel was alien and really didn't like it, so much so that
I immediately took a look at the return policy. In doing so, I plugged my
standard keyboard back in and IMMEDIATELY felt strange. It's almost as if my
hands just knew they belonged on this keyboard, so that's why we still have it
today, and are going to struggle through this learning period as I can already
feel the benefits with my hands. I'll explore different layouts and layer
configs and see what works best for me. So, stick with me and I'll check back
with you in a month!
