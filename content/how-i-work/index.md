---
title: "How I Work"
date: 2021-12-28T10:35:13-07:00
draft: false
showToc: true
cover:
  image: "../../img/how-i-work.webp"
  alt: "Photo by Alex Knight on Unsplash"
tags:
  - productivity
---

I'm always curious what tools others use, as witnessing their workflows allows
me to pick up some things that can help improve my daily experience. Although I
don't spend a lot of time in large codebases, I do manage quite a few little
demo projects covering different languages, build processes, and infrastructure.

Here's how I work on the daily.

## Hardware and Infrastructure

Here's what I use for my workstation and office hardware.

### Laptop

#### Work - Apple MacBook Pro 14" M1 Pro

- 8 CPU/14 GPU (pretty much the base model)
- 16 GB RAM
- 512 GB SSD
- Silver

#### Personal - Apple MacBook Air 13" M1

- 8 CPU/7 GPU
- 8 GB RAM
- 256 GB SSD
- Gold 🤷‍

As a general use laptop, unless you have something against Apple, the Air is
quite possibly the most practical machine for most. Would be cooler if it had a
touchscreen though.

### Monitor

[LG UltraGear 34GP83A-B](https://www.lg.com/us/monitors/lg-34gp83a-b-gaming-monitor)

- 34 Inch 21:9 Curved
- QHD (3440 x 1440)
- 1ms Nano IPS
- 144Hz 👈 once you go high refresh, you can't go back...

I avoided monitors for a bit, since I like the portability of a laptop, but this
thing is a definite luxury to use. Fonts don't look as nice as on the native MBP
screen though 🤷.

### Input

I'm a keyboard nerd and huge fan of layers and ortholinear layouts. So,
basically anything ortho and supports QMK I'm willing to try. Here's my current
dailies.

#### Current Daily Driver

- Corne Wireless

  - Split columnar ortholinear layout
  - Kailh Choc v1
  - Sunset Chocs
    - Tactile
    - 55g actuation @ 1.5mm
    - 3.0mm travel
    - Purpz on spaces and bottom right modifier (shift/enter)
  - MBK Legend keycaps

#### Memorial Wall of Keyboards

- Lily58 Wireless - GOT CORNED

  - Split columnar ortholinear layout
  - Kailh Choc v1
  - Sunset Chocs
    - Tactile
    - 55g actuation @ 1.5mm
    - 3.0mm travel
  - MBK Legend keycaps
  
- Keebio Iris - SOLD

  - Split columnar ortholinear layout
  - Glorious Panda
    - Tactile
    - 67g actuation @ 2.0mm
    - 4.0mm travel
  - /dev/tty MT3 ortholinear keycaps

- OLKB Preonic - SOLD

  - 5 x 12 ortholinear layout
  - Gateron Ink Yellow
    - Linear
    - 60g actuation @ 1.5mm
    - 3.5mm travel
  - NovelKeys PBT Notion cherry keycaps

- YMDK Wings - DISPLAY PIECE

  - 67 key "Arisu" layout
  - Kailh Box Silent Pink
    - Linear
    - 35g actuation @ 1.8mm
    - 3.8mm travel
  - Some amazon japanese wave keycaps

- 🖱️ Logitech G Pro X Superlight
  - Cause why not?

#### Home Row Mods With the Corne

While it's still trial and error, we're leveraging home row mods with ZMK. For
the main modifiers, we're on `tap-unless-interrupted` with some custom
configuration and also are using the `hold-trigger-key-positions` to eliminate
rolling errors when keys are rolled on the same side of the keyboard. The
biggest adjustment has been learning to type properly and use the proper
"shift" key when hitting certain characters like `B`, and `T`. The core of the
layout is based heavily off [Miryoku](https://github.com/manna-harbour/miryoku)
layout. I initially started with the `Shift` modifier on the `A` position, but
found myself still reaching with the pinky finger towards the default shift
position. When I started first looking at the Miryoku layout, I noticed that it
leveraged the stronger pointer finger and figured I'd give it a shot. I'm
honestly still adjusting to it, however, in practice sessions I find myself
making less errors overall. Eventually, we'll work on moving the rest of the
modifiers as well, more specically the `GUI` key (`CMD` on Mac) so I can free
up thumb space for a numbers or media layer. The current challenge is getting
used to the `Backspace` key being on a thumb key. It's taken a bit to even
start using it, however it most definitely helps with minimizing movement. We
use a few combos here, namely `esc` on `QW` and `_` on `,.`. `CAPS WORD` is
super useful, and is currently set to a combo of the home row mods.

### Headphones

I currently use two different noise makers:

#### Sony WH-1000XM3

These over the ear cans are spectacular. Originally bought these for traveling
where the noise cancellation is incredible, and the output is rich.

#### AirPods Pro

I had a pair of the original AirPods, but the Pros are so much nicer. I wasn't
generally a fan of silicon tips, but sometimes when I share an office the ANC is
a nice to have in such a small package. Not to mention they don't mess up your
do.

### Cloud Resources

#### GCP

Most of my systems are hosted in GCP, such as my
GitLab test instances (cloud native in GKE and OmniBus on GCE), my personal
Vault instance, and some extra x86 Ubuntu boxes.

#### AWS

There are a few sample projects deployed to AWS EKS, using CloudFormation stacks
and GitLab CI/CD. Otherwise, I lean more towards GCP.

### Local Infrastructure

#### Docker

The majority of my Docker environments are supported in GCP, since the new M1
architecture is so far not friendly with x86, and not everything works super
well in arm as of yet. I do have some arm64 GitLab Runners that run on the
MacBook Pro, but otherwise try to leverage the ☁️ for most of my compute needs.

Local Docker engine is currently provided by
[Colima](https://github.com/abiosoft/colima).

#### rPi

I currently use a Raspberry Pi 4b with 8GB RAM as a local GitLab Runner box,
since not all of my GitLab accounts have access to shared runners.

There's also an old Raspberry Pi 2B that's still kicking. It's running an arm
version of Docker as well, though it's not used for too much.

## Software

In today's age, pretty much every company is a technology company with software
being so critical to everyday business. A good set of tools can also make your
everyday more efficient as well.

Here's a list of the solutions I use the most:

### VSCode

What can I say, it works. Not the best, not the worst, but for better or worse,
it works. Some of my key extensions include GitLab Workflow and language
specific plugins like Docker, Terraform, etc. GitLab Workflow in particular
enables work specific items like Code Suggestions and Duo Chat.

Theme rotation includes Cattppuccin, Monokai Pro (Octagon), Gruvbox, and some
Synthwave '84 for fun.

### Terminal

I use both VSCode and the terminal, and I've finally spent the time to set up a
terminal environment that fits the same needs. I've primarily been a `Vim`
keybind user since learning \*nix in college, and modal capabilities are welcome
in every solution.

The terminal is used to navigate the file tree and as an easily configurable
development environment. It's the root of where most things start, and will get
a writeup of it's own. My `dotfiles` are available
[here](https://gitlab.com/mrbradleylee/dotfiles).

Here's a list of tools that are terminal based:

- Wezterm for a terminal
  - I've used Alacritty and Kitty as well, they're both really fast
    - Kitty has the best font handling, especially for nerd fonts
    - I prefer Alacritty's new `toml` config
    - Do you want ligatures? [x] Kitty [ ] Alacritty
    - Do you want the best of both worlds? [x] Wezterm
  - [Starship](https://starship.rs) prompt (requires nerdfonts)
    - Font Rotation:
      - Operator Mono - patched for nerd fonts
      - [Victor Mono](https://www.nerdfonts.com/font-downloads)
      - [Fira Code (iScript)](https://github.com/kencrocken/FiraCodeiScript)
- OhMyZSH
- Neovim (LazyVim distro) as an editor
  - Themes change all the time. I like to develop in dark themes, but prefer
    reading in light themes since white bg and dark text are better with
    astigmatisms. That being said, switching between the two can sometimes be a
    challenge, so I prefer dark themes with lighter background colors and lower
    contrast. My current theme rotation:
    - [TokyoNight](https://github.com/folke/tokyonight.nvim)
    - [Catppuccin](https://github.com/catppuccin/nvim)
    - [Edge](https://github.com/sainnhe/edge)
    - [Gruvbox](https://github.com/morhetz/gruvbox)
    - [Monokai](https://github.com/sainnhe/sonokai)
  - Finally swapped over to lua...
    - [dotfiles](https://gitlab.com/mrbradleylee/dotfiles)
    - Switched to LazyVim cause Folke makes good stuff
    - base neovim config in the `nvim.bak` dir
- Tmux as a multiplexer
  - Tried Zellij for a bit, but it took over too many keybinds and ate memory
- GCloud components
  - Helm
  - kubectl
  - kubectx
  - kubens
  - k9s
- Lens for K8S if I'm too lazy for CLI stuff
- Homebrew package manager
- fzf
- ripgrep
- Git
  - [OhMyZSH Git Cheat Sheet](https://kapeli.com/cheat_sheets/Oh-My-Zsh_Git.docset/Contents/Resources/Documents/index)
  - [GitLab Cheat Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
- Docker
  - Enabled on OS X via `Colima`
    - Orbstack required a license for pro use, Colima still rocks for FOSS.
      Orbstack was cool though...
  - Ubuntu/Alpine in GCP for x86

### Browsers

Arc as default browser, Firefox for testing purposes.

If you haven't tried [Arc Browser](https://arc.net/) yet, give it a whirl!
It's Chromium based, and the way it handles tabs and spaces takes a bit to get
used to, but I find it hard to go back to regular Chrome now. It helps keep me
clear of tab hell, while making sure my most used pieces are available in each
space and giving me the flexibility to isolate tabs to specific spaces. Raindrop
has been much more managable for bookmarks than any browser's built in manager.

### Slack

General everyday comms. It can get overwhelming at times, so here's some changes
I made to mute it a bit:

- Sticky `All Unreads` and `All DMs`
- Adjust notifications for important channels
  - Mute my lesser used channels
  - Set mobile notifications for mentions and DMs only
  - Learn keyboard shortcuts

### 1Password

Nothing much needs to be said here. Password managers are 👨‍🍳💋. 1Password
manages not only my passwords, but my SSH keys and newly supported passkeys.
Having a centralized place that I can manage "device" specific keys to use
across multiple devices is fantastic, and SSH key rotation is so easy.

### Alfred

Replaces Spotlight. I like the speed, more precise results, and the Workflows.

I replaced the default Spotlight keybind, `cmd + space`. Alfred's free version
is already quite powerful as a launcher and fuzzy finder, however the paid
version unlocks Workflows, which I use on a daily basis and personally are worth
the investment. For example, `gi` searches issues in the `gitlab-org` group, and
`issues` searches my personal group for issues.

### Middle

Speaking of the trackpad, as mentioned before, I like portability and therefore
try to work and travel with as little as possible, while trying to remain
remotely ergonomic. The MacBook Magic keyboard works well enough, however the
trackpad has no middle click option, and `fn` click is not entirely useful. I
still use keyboard functions as much as possible, but sometimes that middle
click is just faster, which is where middle comes in. I currently have my middle
click mapped to a three finger tap.

## More To Come

This is just a short list of tools and solutions that I touch everyday. As
things change or new solutions come in, we'll be sure to keep them updated here.
Hopefully seeing how I work helps make things a little more efficient for you!
