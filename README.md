# mrbradleylee.com

![logo](static/img/meme.webp)

## Usage

Built with **Hugo**

Hosted in **GitLab Pages**

### Install Hugo

OSX

```shell

brew install hugo

```

### Dev

```shell
hugo server -D
```

The `-D` is to include drafts in the build. Served on port `1313`

### Build

```shell

hugo

```

## Content

Default content dirs

### Output

`public/`

### Theme

PaperMod: installed as a submodule

#### Update

On reclone: `git submodule update --init --recursive`

To update: `git submodule update --remote --merge`

Don't modify themes directly, instead override matching elements in:
`layouts/partials/`

### Adding New Content

`hugo new <type>/<name>`

eg: `hugo new posts/postname.md`

#### Location

`content/`

#### Static Assets

Objects in the `static` directory (excluding `themes`) will get
copied to the root on build.

`static/favicon/`, `static/img/`
