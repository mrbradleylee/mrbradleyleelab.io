---
title: "Style Guide"
date: 2021-07-27T11:50:13-07:00
lastmod: 2021-07-27T11:50:13-07:00
draft: true
---

This is a reference doc for the markdown syntax used to render the content here.
This doc moves with me from project to project.

---

## Headers

Cascading levels using `#`, for example:

```md
# This is an H1

## This is an H2 header

### This is an H3 header
```

<br>

# This is an H1

## This is an H2 header

### This is an H3 header

## Paragraphs

Standard paragraph text. Line break coming up.

Next paragraph in the list.

You can also use `<br>` as a line break if needed.

## Emphasis

This paragraph contains `**bold text**` > **bold text**.

This line contains `_italic text_` > _italic text_.

And this one is `**_bold and italic_**` > **_bold and italic_**.

## Blockquotes

```md
> Can contain **bold**, _italic_, and **_bold italic_**.
```

> Can contain **bold**, _italic_, and **_bold italic_**.

### Blockquotes with multiple paragraphs

```md
> This blockquote contains
>
> multiple paragraphs.
```

> This blockquote contains
>
> multiple paragraphs.

### Nested blockquotes

```md
> This blockquote has
>
> > a nested blockquote.
```

> This blockquote has
>
> > a nested blockquote.

### Blockquotes with other elements

```md
> ## H2 in a blockquote as a title!
>
> - First list object here.
> - Followed by a second list object.
>
>   _Everything_ is going according to **plan**.
```

> ## H2 in a blockquote as a title
>
> - First list object here.
> - Followed by a second list object.
>
>   _Everything_ is going according to **plan**.

## Lists

### Ordered Lists

Have the lists numbered, they don't need to be in order specifically, Just need
to start with 1.

#### Standard ordered lists

```md
1. First
2. Second
3. Third
4. Fourth
```

1. First
2. Second
3. Third
4. Fourth

<br>

#### Nested ordered lists

```md
1. First
2. Second
3. Third
   1. Third First
   2. Third Second
4. Fourth
```

1. First
2. Second
3. Third
   1. Third First
   2. Third Second
4. Fourth

### Unordered Lists

Dashes `-` or Asterisks `*` in front. Code formatters will usually override to
it's preferred default.

```md
- one
- two
- three
- four

* one
* two
* three
* four
```

- one
- two
- three
- four

<br>

- one
- two
- three
- four

### Adding Elements In Lists

#### Paragraph in the middle of a list

```md
- This is the first list item.
- Here's the second list item.

  I need to add another paragraph below the second list item.

- And here's the third list item.
```

- This is the first list item.
- Here's the second list item.

  I need to add another paragraph below the second list item.

- And here's the third list item.

#### Blockquotes in the middle

```md
- This is the first list item.
- Here's the second list item.

  > A blockquote would look great below the second list item.

- And here's the third list item.
```

- This is the first list item.
- Here's the second list item.

  > A blockquote would look great below the second list item.

- And here's the third list item.

### Code

Codeblocks are denoted with `` ` ``

Single backticks present `inline code`

`` They can be `escaped` with double backticks ``

### Codeblocks

Can be indented, but we'll stick to fencing with `grave` or `backtick`.

For example:

````md
```
code goes here
```
````

```javascript
// sample js script
function House(rooms, apartment, color, city, state) {
  this.rooms = rooms;
  this.apartment = apartment;
  this.color = color;
  this.city = city;
  this.state = state;
}

const houseOne = new House(3, false, "brown", "Phoenix", "AZ");
const houseTwo = new House(6, false, "white", "Honolulu", "HI");
const houseThree = new House(1, true, "purple", "Bend", "OR");
```

### Links

To create a link, enclose the link text in brackets (e.g., `[Duck Duck Go]`) and
then follow it immediately with the URL in parentheses (e.g.,
`(https://duckduckgo.com)`).

Titles are included by `" "` in the url `()`. You can see it on hover. `< >`
create email links, we'll not do that here...

```md
[sample link to google](https://www.google.com "This is the title")
```

[sample link to google](https://www.google.com "This is the title")

### Images

Images can be referenced with `!`. In this example, the alt text comes first,
within the `[ ]`, and the mouseover title follows the path and is noted in
`" "`.

```md
[![An old rock in the desert](/path/to/image.jpg "Shiprock, New Mexico by Beau Rogers")](link
if needed)
```
