---
title: "mrbradleylee.com"
date: 2021-04-13T13:22:27-07:00
lastmod: 2021-04-13T13:22:27-07:00
draft: true
tags:
  - hugo
  - gitlab
  - gitlab pages
  - netlify
  - productivity
  - 11ty
---

Personal portfolio and project for front-end practice. The goal is to retain
fast loading and build times. It'll most likely change quite a bit. 😄

The [changelog](#changelog) section details how it's grown and changed over
time. The latest iteration was made to simply play around with other SSGs. 11ty
is great, but personally I like Hugo better as the configuration is a bit more
straightforward and it definitely builds _fast_.

## Source

[Live 🤔](/) | [Repo](https://gitlab.com/mrbradleylee/mrbradleylee.gitlab.io)

## Technologies

- Hugo
- Currently hosted in GitLab Pages
- Neovim in Alacritty

## Changelog

See how it's changed over time. Here's a running list of the different
implementations over time, newest to oldest.

### v1.mrbradleylee.com

[Live](https://mrbradleylee.gitlab.io/v1.mrbradleylee.com/) |
[Repo](https://gitlab.com/mrbradleylee/v1.mrbradleylee.com)

Since I was still revisiting CSS at the time, I didn't want any prepackaged
themes, so it's pretty spartan, but was a great learning experience for using
flexboxes and more practice with SSGs.

This was also my first experience with proper cloud based CI/CD with Netlify.

- Built with 11ty
- Currently hosted in GitLab Pages
  - Originally built and hosted in Netlify

### moostache.io

[Live](https://mrbradleylee.github.io/moostache.io) |
[Repo](https://github.com/mrbradleylee/moostache.io)

This was my first _real_ foray into SSGs, and I definitely didn't have a clue
what I was doing. I pulled this project again and when fixing the links for a
move to GitHub Pages, I appreciated how easy it was to update all the links to
reflect the new path. One of the main reasons to use an SSG for a multi page
website.

All iterations of the `moostache.io` domain were hosted locally on an
[rPi](https://github.com/mrbradleylee/rPi) using Docker containers.

- Built with Docusaurus
- Currently Hosted in GitHub Pages
  - Deployed through `npm run deploy`
  - Originally self hosted on an rPi using Docker

### v1.moostache.io

[Live](https://mrbradleylee.github.io/v1.moostache.io/) |
[Repo](https://github.com/mrbradleylee/v1.moostache.io)

This project originated as a way to build something pure, simply HTML and CSS.
That went off the deep end when I started looking at how to make the site
responsive, which eventually led to the SSG train that happened after. If you
dig into the code for a few, you'll definitely see the `media` queries and how
they've changed over time. While SSGs make it easy, a project like this made it
possible to learn the building blocks. Also, looks like the parralax is broken
in the live. RIP...

- Almost pure HTML/CSS/JS
- Uses Sergey SSG for some simple HTML replacement
- Hosted in GitHub Pages
  - Deployed with `git subtree --prefix public origin gh-pages`
    - This requires the build to be done locally and also under source control
  - Originally self hosted on an rPi using Docker
