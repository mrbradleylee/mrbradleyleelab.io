---
title: "Moonlander Update"
date: 2022-04-04T12:56:42-07:00
lastmod: 2022-04-04T12:56:42-07:00
showToc: true
tags:
  - moonlander
  - keyboards
  - ortholinear
---

Exactly one calendar month ago, I got my hands on a [ZSA
Moonlander]({{< ref "moonlander.md" >}}) and I also promised an update in a
month, so let's see how we made out.

## Did I Survive?

For the TLDR; version, yes and no. The Moonlander did not survive a month, in
fact it only made it two weeks before being sold off. Was it terrible? Did it
break? Does the thing just suck? In my experience, none of the above. In fact,
the experience was fantastic and opened my eyes to a whole new world of
keyboards. I'm sure you're wondering, "if that's the case, why get rid of it?"
And that's a great question, and exactly what we'll dive into today.

## Why The Moonlander?

Rather than a glorious copy pasta from the last article, in short, I type a lot
and want to get ahead of some pains that I was starting to feel. In addition to
what's listed there, I got to think a bit more about why I chose it, chief among
those new reasons is that it's one of the few ergonomic options that you can
just _buy_. And that's a big reason, since building introduces it's own issues
with you know, building a keyboard, and warranties et al. Out of they promise
you a keyboard that can fit every hand and position and give you the ergonomic
experience dreams are made of. I added a little flavor there, of course, but
that's basically what's advertised. For the most part, that's a hit, but there's
one glaring issue that I had with it...

### What I Liked

Before we get into the issues, let's talk about the benefits, and what I
actually enjoyed about the Moonlander. Two main things come to mind here:
columnar/ortholinear layout, and the split.

#### The Columnar/Ortho Layout

The accuracy of the origin of staggered layouts is debatable, but for the most
part, we agree that it stems from the typewriter and having to make room for the
arms that slapped up onto the paper. Now, to put it simply, when your fingers
are on the keyboard, they move up and down very freely, but contorting them to
move diagonally just isn't natural. That's where the columnar/ortholinear
layouts come into play. We won't get into the details of the differences betwixt
the two, but just know there's some out there that will differentiate them.
We'll simply refer to it as `ortho` here.

The ortho layout basically removes the stagger, and puts the keys on a grid.
This was one of the largest adjustments when first making the switch to the
Moonlander. While this isn't really an ergonomic advantage, per se, think about
learning to touch type and using a num pad. Learning to type was most likely
much harder than learning how to use a num pad without looking, and I feel this
is largely due to the layout of the num pad. It's a grid, and you know, if this
finger is on 5, the 8 is right above it. Maybe not the most glamorous example,
but if you think about it, it makes sense, and you're applying that logic to the
rest of the keyboard. Quick, think what key `3` is above? What'd you end up
with? `W` or `E`? On a stagger, you're not wrong either way, on ortho, it's `E`,
`2` is over `W`, so on and so forth. When we look at it that way, things start
to make sense. I can say, that after the initial adjustment period, I'm finding
the ortho layout incredibly comfortable and intimately familiar, like it should
have been this way all along (Apple, ortho MacBook when?). The biggest advantage
I think it offers me, is that my fingers simply move less, and that makes typing
a much more efficient experience. This is also another way the ortho layout
helps/prevents pain, by reducing overall hand movement. Trust me, going back to
stagger from ortho feels frantic...

#### The Split

Yes, the split. The keyboard is physically split in two pieces, which can be
close together or far apart. I'm finding that just inside shoulder width is the
most comfortable for me. Between the split and ortho, I initially thought what
made more of the difference was the ortho layout over the split for being
comfortable, but after experimenting with a Preonic (50% or 5x12 ortho layout)
that I indeed missed the split. I can actually find comfortable positions for
the armrests on my chair and can most definitely feel the discomfort in my
shoulders and hands (and posture in general) when I have to use a normal
keyboard. For pure ergonomics, I don't think you can beat a split keyboard.
Fortunately, if you want to try it, there's a few that just use a standard
staggered layout, like the [Dygma Raise](https://dygma.com/). With the split it
is so easy to put the keyboard in a much more comfortable position I can't
imagine using anything else regardless of key layout.

#### The Layers

Who knew keyboards could be like onions? I mean, we experience this in all
keyboards with `Shift` for example making a `3` into `#`. Now what if you could
take that `3` and also make it a `-` and a `_`, and also an `=` and a `+` just
by hitting a different modifier? That, my friends, is what layers do. Most
custom built keyboards will be fully programmable via the QMK firmware, which
allows over a dozen different layers to be programmed, each with individual key
assignments, combo mod keys, and even macros. This is how we get more usable
functionality out of less keys. When you couple this with the ortho layout, you
can enter the majority of your keypresses without moving any of your fingers
farther than one key. When it comes to speed, most will lose it by making
mistakes, and most mistakes are made when you have to leave the home row and you
have to find your way back, so having everything within reach indirectly
increases speed through accuracy.

This comes in incredibly handy with symbols and brackets, of which all of the
ones I use the most `-=(){}[]_+` are all available really close to the home row
which takes advantage of the minimal movement the ortho layout offers.

#### The Rest

There's much more that I did enjoy about it, but are not as important as the
layout and split. They are still worth mentioning though, and in no specific
order:

- QMK Firmware (based on ZSA's own fork `Oryx`)
- Four key thumb cluster
- Tenting ability
  - This one deserves an honorable mention, as this singular feature helps
    alleviate different types of pain and is arguably as important as the split
- Hot swappable switches (I tested a few, lol)

### What I Didn't Like

Of course, it's not all sugar and rainbows, and there are a few key challenges
with the Moonlander that ultimately led to it's demise. I will say, as with any
new layout, you'll need time to adjust, so we'll ignore the initial adjustment
period for the ortho layout. Let's just say the `xcv` cluster took a couple of
weeks to get used to. We'll start with the most major challenge...

#### Tenting And The Thumb Cluster

One of the most interesting features of a split keyboard is giving new
responsibilities to your thumbs, probably the strongest of your phalanges. On a
standard layout it's delegated to the space bar and the occasional `Win` or
`Alt` use. With the Moonlander, each thumb cluster has four keys that can be
programmed to basically anything. You're thinking now, "if it's that cool, why
is it under what I didn't like?" You'd be thinking correctly, and that's where
we can talk about tenting, and the physical design of the Moonlander itself.

In short, tenting is basically lifting the inside of each side to produce an
angle, which keeps your hands in a more natural position by removing pronation.
Think about holding a ball vs laying your hands flat on the table. It's actually
quit comfortable in practice, and I think will be very valuable with my current
keyboard. Here's the challenge I had with the Moonlander: it uses the thumb
cluster as a support for tenting. What this means is that the higher you go, the
larger the distance between the alpha keys and the thumb cluster. If you have
larger hands, or maybe just a different shape like longer thumbs, it may not
matter to you. However, in the most comfortable tenting position, my thumbs
ended up in what's known as the "ErgoDox Death Zone," or basically in between
the keys. When my thumbs lay properly on the top most cluster key, I felt like I
was stretching to reach anything other than the single key, and that made the
other three keys on the cluster basically useless for high volume presses like
layer switching. This was the number one reason it didn't work for me, and had
me searching for other solutions.

#### More Cons

Besides the tenting, there's a few more cons, that mattered, but were not as
intrumental in moving away from it. Again, in no particular order:

- Too many keys, most were just never used
- Awkward thumb cluster positions, even in the neutral state some are hard to
  press
- Price...

## What's Next?

While the Moonlander didn't work, it did show me what I liked and wanted out of
my "endgame" keyboard for productivity: ortho and split, with QMK support. We
took a few steps to get there, going from the Moonlander, to a 67 key "Arisu"
layout, to the 5x12 Preonic, and finally where we are today, with the Keebio
Iris, which is basically a split Preonic, with 56 total keys, and support for
tenting ironically using the ErgoDox tenting legs that are on the Moonlander.

Through the journey we tried multiple keycap and switch types to see what was
comfortable, usable, and looked good 😝. Spoiler alert, we ended up with
Glorious Pandas, which are tactile at the top, with a 67g actuation force and a
2.0mm actuation distance. I didn't think I'd like them, since I wasn't a huge
fan of the Cherry Browns that came with the Moonlander, but that writeup can be
another day. Let's just say I didn't think I'd be in tactile gang again and am
glad I tried them out. I do still use the Alice keyboard for gaming, and those
are equipped with some speedy linears which serve that purpose well.

### Current Loadout

With all that being said, here's the current stable:

- Keebio Iris

  - This is my main productivity keyboard
  - Split columnar 56 key ortholinear layout
  - Glorious Panda
    - Tactile
    - 67g actuation @ 2.0mm
    - 4.0mm travel
  - /dev/tty MT3 ortholinear keycaps

- OLKB Preonic

  - Backup ortho, it's tiny
  - 5 x 12 ortholinear layout
  - Gateron Ink Yellow
    - Linear
    - 60g actuation @ 1.5mm
    - 3.5mm travel
  - Keycapless, lol...

- YMDK Wings

  - The gaming keyboard, since games like standard layouts...
  - 67 key "Arisu" layout
  - Kailh Box Silent Pink
    - Linear
    - 35g actuation @ 1.8mm
    - 3.8mm travel
  - NovelKeys PBT Notion Cherry

These are just the customs... I also have a couple of backups which include an
older Logitech G-Pro (the Romer G switches are FANTASTIC) and a Varmilo VA87M
running Cherry MX Silvers.

As you can see, sometimes building and tinkering with keyboards can become a
hobby. Let me warn you right now, DO NOT GET INTO MECHANICAL KEYBOARDS. It's a
rabbit hole, and spendy one at that... I can guarantee you'll never be satisfied
with a keyboard again in your life, but you can definitely build almost anything
that fits your needs and desires, and at the end of the day, that's what I
learned most about this journey. It's been fun building the different options
and experiencing the different types, and I look forward to trying more when the
wallet recovers... Until then, I wish you the best of luck finding the one that
works best for you!
